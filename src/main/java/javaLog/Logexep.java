package javaLog;

import org.apache.log4j.Logger;

public class Logexep {

    private final Logger logger = Logger.getLogger(Logexep.class);
    private static final String MESSAGE_SUCCESS = "Application launched successfully";
    private static final String MESSAGE_EXCEPTION = "Generated number - ";

    public boolean generateError(int num) throws LessThanSixException {
        if (num <= 5) {
            logger.error(new LessThanSixException(MESSAGE_EXCEPTION + num));
            return true;
        } else {
            logger.info(MESSAGE_SUCCESS);
            return false;
        }
    }

    static class LessThanSixException extends Exception {
        public LessThanSixException(String message) {
            super(message);
        }
    }
}


