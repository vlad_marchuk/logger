package javaLog;

    import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;

    public class LogexpepTest {
        Logexep cut = new Logexep();
        static List<Arguments> generateErrorTestArgs(){
            return List.of(
                    Arguments.arguments(2, true),
                    Arguments.arguments(7, false)
            );
        }

        @ParameterizedTest
        @MethodSource("generateErrorTestArgs")
        void generateErrorTest(int num, boolean expected) throws Logexep.LessThanSixException {
            boolean actual = cut.generateError(num);
            Assertions.assertEquals(expected, actual);
        }


    }

